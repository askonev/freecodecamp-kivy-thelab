from kivy.uix.screenmanager import ScreenManager


class NavigationScreenManager(ScreenManager):
    screen_stack = []

    def push(self, screen_name: str):
        if screen_name in self.screen_stack:
            return
        self.screen_stack.append(self.current)
        self.transition.direction = "left"
        self.current = screen_name

    def pop(self):
        if not self.screen_stack:
            return
        self.transition.direction = "right"
        self.current = self.screen_stack.pop()
