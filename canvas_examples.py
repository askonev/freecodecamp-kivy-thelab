from kivy.graphics.context_instructions import Color
from kivy.graphics.vertex_instructions import Line, Rectangle, Ellipse
from kivy.lang import Builder
from kivy.metrics import dp
from kivy.properties import Clock
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.widget import Widget

Builder.load_file("canvas_examples.kv")


class CanvasExample1(Widget):
    pass


class CanvasExample2(Widget):
    pass


class CanvasExample3(Widget):
    pass


class CanvasExample4(Widget):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        with self.canvas:
            Line(points=(100, 100, 400, 500), width=2)
            Color(0, 1, 0)
            Line(circle=(400, 200, 80), width=2)
            Line(rectangle=(700, 500, 150, 100), width=2)
            self.rect = Rectangle(pos=(500, 200), size=(150, 100))

    def on_button_a_click(self, widget):
        x, y = self.rect.pos
        w, h = self.rect.size
        inc = dp(10)

        diff = self.width - (x + w)
        if diff < inc:
            inc = diff
        x += inc

        self.rect.pos = (x, y)

class CanvasExample5(Widget):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.ball_size = dp(50)
        self.ball_vx = dp(3)
        self.ball_vy = dp(4)
        with self.canvas:
            self.ball = Ellipse(
                pos=(100, 100),
                size=(self.ball_size, self.ball_size))
        Clock.schedule_interval(self.update, 1 / 60)

    def on_size(self, *args):
        # print("on size : " + str(self.width) + ", " + str(self.height))
        self.ball.pos = (
            self.center_x - self.ball_size / 2,
            self.center_y - self.ball_size / 2)

    def update(self, dt: int):
        x, y = self.ball.pos
        x += self.ball_vx
        y += self.ball_vy

        if x + self.ball_size >= self.width:
            x = self.width - self.ball_size
            self.ball_vx = - self.ball_vx
        elif x <= 0:
            x = 0
            self.ball_vx = - self.ball_vx

        if y + self.ball_size >= self.height:
            y = self.height - self.ball_size
            self.ball_vy = - self.ball_vy
        elif y <= 0:
            y = 0
            self.ball_vy = - self.ball_vy

        self.ball.pos = (x, y)


class CanvasExample6(Widget):
    pass


class CanvasExample7(BoxLayout):
    pass
