from kivy.lang import Builder
from kivy.properties import StringProperty, BooleanProperty
from kivy.uix.gridlayout import GridLayout

Builder.load_file("widget_examples.kv")


class WidgetsExample(GridLayout):
    SLIDER_INIT_VALUE = 0
    DEFAULT_TEXT = "Hello!"
    my_text = StringProperty(DEFAULT_TEXT)
    toggle_button_init_text = StringProperty("Off")
    label_input_text = StringProperty(DEFAULT_TEXT)
    is_count_enabled = BooleanProperty(toggle_button_init_text == "On")
    # Used slider.id instead
    # slider_value_txt = StringProperty(f"{SLIDER_INIT_VALUE}")
    count = 0

    def on_button_click(self):
        print(f"Count enabled: {self.is_count_enabled}")
        if not self.is_count_enabled:
            return
        self.count += 1
        self.my_text = str(self.count)
        print("Button clicked")

    def on_toggle_button_state(self, widget):
        print("Toggle state " + widget.state)
        if widget.state == "normal":
            self.toggle_button_init_text = "Off"
            self.is_count_enabled = False
        else:
            self.toggle_button_init_text = "On"
            self.is_count_enabled = True

    def on_switch_active(self, widget):
        print(f"Switch: {widget.active}")

    def on_my_text_input_validate(self, widget):
        self.label_input_text = widget.text


    # Used ID in .kv file instead
    # def on_slider_value(self, widget):
    #     self.slider_value_txt = str(int(widget.value))
    #     print(f"Slider: {widget.value}")

